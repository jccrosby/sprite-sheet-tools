<?php
class SpriteSheet {
	
	protected $ffmpegCmd = "ffmpeg";
	protected $montageCmd = "montage";
	protected $identifyCmd = "identify";
	
	public function __construct() {
		echo("Created SpriteSheet() object");
	}
	
	public function createSpriteSheet($input, $interval, $outputName, $extension, $width, $height, $keepimages) {
		$pathValList = pathinfo("output/" . $outputName . "/" . $outputName . "." . $extension);
		$dir = $pathValList["dirname"];
		$fileName = $pathValList["filename"];
		$fileExt = $pathValList["extension"];
		
		if(!file_exists($dir)) {
			mkdir($dir, 0777, true);
		}
		echo("cmd: " . $this->ffmpegCmd);
		// Create the thumbnails
		$ffmpegCommand = $this->ffmpegCmd . " -i " . $input . " -f image2 -vf fps=fps=1/" . $interval . " " . $dir . "/" . $fileName . "-%03d" . "." . $fileExt;
		echo "\nffmpeg command: " . $ffmpegCommand . "\n";
		exec($ffmpegCommand);
		
		// Create the spritesheet
		$montageCommand = $this->montageCmd . " " . $dir . '/' . $fileName . "-*." . $fileExt . " -geometry \"" . $width . "x" . $height . ">+0+0\" -background none output/" . $fileName . "-sheet." . $fileExt;
		echo "\nmontage command: " . $montageCommand . "\n";
		exec($montageCommand);
		
		// Clean up unless we're told to do otherwise :)
		if(!$keepimages) {
			if(file_exists($dir)) {
				$this->_rmdirRecursive($dir);
			}
		}
	}

	public function createVTTFile($spriteSheetPath, $vttFile, $vttPrefix, $interval, $duration, $width, $height) {
		/*
		// If the vttFile direcotry doesn't exist, created it.
		$pathValList = pathinfo($vttFile);
		$dir = $pathValList["dirname"];
		if(!file_exists($dir)) {
			mkdir($dir, 0777, true);
		}*/
		
		$pathValList = pathinfo($spriteSheetPath);
		$spriteSheetFileName = $pathValList["filename"] . "." . $pathValList["extension"];
		
		$identOut = exec($this->identifyCmd . " " . $spriteSheetPath . "\n");
		$identValList = explode(" ", $identOut);
		
		$imageExt = $identValList[1];
		
		$imgSize = $identValList[2];
		$imgSizeValList = explode("x", $imgSize);
		$imgWidth = $imgSizeValList[0];
		$imgHeight = $imgSizeValList[1];
		
		$rows = $imgHeight/$height;
		$columns = $imgWidth/$width;
		
		echo("Creating the VTT file (" . $vttFile . ")\n");
		echo("for " . $spriteSheetFileName . " (" . $imgWidth . "x" . $imgHeight . ") spritesheet with\n");
		echo("with " . $rows . " rows and " . $columns . " columns...\n");
		
		// Start the actual content 
		$currentTime = $interval;
		$row = 0;
		$column = 0;
		$start = $end = "00:00.000";
		
		$vttContents = "";
		
		// file writes
		echo("Building VTT file (" . $vttFile . ")...\n");
		if(file_exists($vttFile)){ // Remove the file if it exists
			unlink($vttFile);
		}
		
		// write the header
		file_put_contents($vttFile, "WEBVTT\n\n", FILE_APPEND);
		
		// Write the timecoded contents with positions
		$building = true;
		while($building) { 
			
			$sheetX = $width * $column;
			$sheetY = $height * $row;
			$sheetWidth = $imgWidth;
			$sheetHeight = $imgHeight;
			
			// timecode
			//$vttContents .= $currentTime . "\n";
			$end = $this->_convert_seconds_to_time_code($currentTime);
			file_put_contents($vttFile, $start . " --> " . $end . "\n", FILE_APPEND);
			$start = $end;
			
			// asset info
			//$vttContents .= $vttContents . $spriteSheetPath . "#xywh=" . $sheetX . "," . $sheetY . "," . $sheetWidth . "," . $sheetHeight . "\n";
			file_put_contents($vttFile, $vttContents . $vttPrefix . $spriteSheetFileName . "#xywh=" . $sheetX . "," . $sheetY . "," . $width . "," . $height . "\n", FILE_APPEND);
			file_put_contents($vttFile, "\n", FILE_APPEND);
			
			$column++;
			if($column >= $columns) {
				$sheetX = 0;
				$sheetY = $imgHeight;
				
				$column = 0;
				$row++;
			}
			
			$currentTime = $currentTime + $interval;
			//echo("Building: " . $building . " col: " . $column . " row: " . $row . "\n");
			if($building && 
			   ($row >= $rows) || // If we are out of rows or if we are out of time
			   ($currentTime >= $duration)) {
				$building = false;
			}
		}
		
		echo("...done!\n");
		exit(1);
	}
	
	
	// Utils

	private function _convert_seconds_to_time_code($seconds) {
		$hours = 0;
		$milliseconds = str_replace( "0.", "", $seconds - floor( $seconds ) );

		if($seconds > 3600) {
			$hours = floor($seconds / 3600);
		}
		$seconds = $seconds % 3600;

		return str_pad($hours, 2, '0', STR_PAD_LEFT)
			.gmdate( ':i:s', $seconds ) . "."
			.str_pad($milliseconds, 3, "0", STR_PAD_LEFT)
		;
	}

	private function _rmdirRecursive($dir) {
	    foreach(scandir($dir) as $file) {
	        if ('.' === $file || '..' === $file) continue;
	        if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
	        else unlink("$dir/$file");
	    }
	    rmdir($dir);
	}


	// Docs

	public function echo_all_usage() {
		$this->echo_create_spritesheet_usage();
		$this->echo_create_vtt_usage();
	}		

	public function echo_create_vtt_usage() {
		echo "\n";
		echo "Usage: $ php spritesheet.php createVTT {path_to_sprite_sheet} {path_to_vtt_output} {prefix_for_vtt_asset} {thumb_interval} {video_duration} {thumb_width} {thumb_height} \n";
		echo "${path_to_sprite_sheet} - file path to the sprite sheet image. \n"; 
		echo "${path_to_vtt_output} - file path where the VVT file should be saved. Directory must exist. If the file exists, it will be overwritten. \n";
		echo "${prefix_for_vtt_asset} - prefix in the VTT file for the image asset. Ex: assets/images/ \n";
		echo "${thumb_interval} - the time interval in seconds for which the thumbnails were created. \n";
		echo "${video_duration} - the duration of the video in seconds. \n";
		echo "${thumb_width} - the width of the thumbnail image in pixels. \n";
		echo "${thumb_height} - the height of the thumbnail image in pixels. \n\n";
	}

	public function echo_create_spritesheet_usage() {
		echo "\n\n*** REQUIRES ffmpeg (http://ffmpeg.org/) and ImageMagick (http://www.imagemagick.org/)\n\n";
		echo "Usage: $ php spritesheet.php createSpriteSheet {input} {interval} {thumb_width} {thumb_height} {output_path} keepimages \n";
		echo "${input} - path to the video to create the sprite sheet from. \n"; 
		echo "${width} - width of the thumbnails in pixels. \n"; 
		echo "${height} - height of the thumbnails in pixels. \n"; 
		echo "${interval} - the interval to capture frames at in seconds\n";
		echo "${output_name} - name to give to the files. Example spritesheet \n the thumbbs will be created in output/spritesheet-%03d.png\n and the spritesheet file will be named spritesheet-sprite.png";
		echo "keepImages (optional) - if added the images used to create the spritesheet will not be deleted.\n\n";
	}

}


// ======================================
// Command Line Usage
// ======================================
if(PHP_SAPI === "cli") {
	
	$spriteSheet = new SpriteSheet();
	
	if($argc <= 4) {
		$spriteSheet->echo_all_usage();
		exit(1);
	}
	
	switch($argv[1]) {
		case "createSpriteSheet": {
			$input = $argv[2];
			$interval = $argv[3];
			$width = $argv[4];
			$height = $argv[5];
			$outputName = $argv[6];
			$extension = $argv[7];
			
			if($argc == 9 && $argv[8] == 'keepimages') {
				$keepimages = true;
			} else {
				$keepimages = false;
			}
			
			$spriteSheet->createSpriteSheet($input, $interval, $outputName, $extension, $width, $height, $keepimages);
			break;
		}
		case "createVTT": {
			// TODO: error handling for arguments, files, & paths that don't exist
			$spriteSheetPath = $argv[2];
			$vttFile = $argv[3];
			$vttPrefix = $argv[4];
			$interval = $argv[5]; // time between thumbs in seconds
			$duration = $argv[6]; // duration in seconds
			$width = $argv[7]; // width of the image
			$height = $argv[8]; // height of the image
			$spriteSheet->createVTTFile($spriteSheetPath, $vttFile, $vttPrefix, $interval, $duration, $width, $height);
			break;
		}
		default: {
			$spriteSheet->echo_all_usage();
		}
	}	
}
?>