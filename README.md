# Sprite Sheet Tools - PHP (WIP)

## Overview
PHP CLI script for creating spritesheets for video as well as VTT data for image timing. 

## Requirements
* PHP 5.3+
* ffmpeg <http://www.ffmpeg.org>
* ImageMagick <http://www.imagemagick.org/>

## Usage
### Create a spritesheet and VTT file
	$ php spritesheet.php createSpriteSheet ${input} ${interval} ${thumb_width} ${thumb_height} ${output_path} keepimages 
	
* ${input} - path to the video to create the sprite sheet from.  
* ${width} - width of the thumbnails in pixels.  
* ${height} - height of the thumbnails in pixels.  
* ${interval} - the interval to capture frames at in seconds
* ${output_name} - name to give to the files. Example spritesheet the thumbbs will be created in output/spritesheet-%03d.png\n and the spritesheet file will be named spritesheet-sprite.png";
* keepImages (optional) - if added the images used to create the spritesheet will not be deleted.

**Example:**

	$ php spritesheet.php createSpriteSheet bigbuck.mp4 5 127 74 bigbuck png
	
*The example above will create a Sprite sheet & VTT file based on a with thumbs every 5 seconds that are 127 wide x 74 high and a video duration of 596 seconds*

### Create a VTT file from an existing spritesheet

	$ php spritesheet.php createVTT ${path_to_sprite_sheet} ${path_to_vtt_output} ${prefix_for_vtt_asset} ${thumb_interval} ${video_duration} ${thumb_width} ${thumb_height} 

* ${path_to_sprite_sheet} - file path to the sprite sheet image.  
* ${path_to_vtt_output} - file path where the VVT file should be saved. Directory must exist. If the file exists, it will be overwritten. 
* ${prefix_for_vtt_asset} - prefix in the VTT file for the image asset. Ex: assets/images/ 
* ${thumb_interval} - the time interval in seconds for which the thumbnails were created. 
* ${video_duration} - the duration of the video in seconds. 
* ${thumb_width} - the width of the thumbnail image in pixels. 
* ${thumb_height} - the height of the thumbnail image in pixels.

**Example:**

	$ php spritesheet.php createVTT output/bigbuck-sheet.png output/bigbuck.vtt assets/images/ 5 596 127 74

*The example above will create a VTT file based on a sprite sheet with thumbs every 5 seconds that are 127 wide x 74 high and a video duration of 596 seconds*

## Licensing

TBD
